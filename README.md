# D-Multi-PlexR 

D-Multi-PlexR.sh requires two inputs: 

* Fastq file 
* File containing all primer and barcode combinations

The output is:

A folder containing fastq's with filenames that contain the barcode and primer sets that genertated the file.

The fastq output in each file contains sequences that have been trimmed of the primer and the barcode.

Notes on this version:

* Barcodes from this project had three mismatches before the beginning of the forward barcode and after the end of the reverese barcode.
* Only Y,W degenerate values were used from the (IUPAC) code, more can be added.
* Trimming is now done with the identification of the position of the beginning of the forward and reverse primers.

Usage: 

```{bash}

	D-Multi-PlexR.sh example.keyfile.txt path/to/FastqDirectory/*.fastq

```

An example of input for the barcode and primer set can be found in [example.keyfile.txt](example.keyfile.txt)

This software is licensed under [MIT License.](license.txt)

This software utilizes [agrep](https://github.com/Wikinaut/agrep). Users of this software should acknowledge this license as well.

		AGREP - approximate GREP for fast fuzzy string searching. 
		Files are searched for a string or regular expression, with approximate matching capabilities and user-definable records. 
		Developed 1989-1991 by Udi Manber, Sun Wu et al. at the University of Arizona. 
		ISC open source license since Sept. 2014."


